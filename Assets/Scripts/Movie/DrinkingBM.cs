﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkingBM : MonoBehaviour
{
    [SerializeField] private GameObject targetGameObject;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        PcamEndCheck();
    }

    void PcamEndCheck()
    {
        if (targetGameObject.activeSelf) UpdateAnimator();
        //Debug.Log("active" + targetGameObject.activeSelf);
    }

    void UpdateAnimator()
    {
        animator.SetBool("Drinking", true);
    }
}
