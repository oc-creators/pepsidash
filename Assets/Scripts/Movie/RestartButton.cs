﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class RestartButton : MonoBehaviour
{
    public PlayableDirector playableDirector;
    public void pause()
    {
        playableDirector.Pause();
    }
    public void restart()
    {
        playableDirector.Resume();
    }
}
