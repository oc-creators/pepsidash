﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Movie
{
    public class Winningwalk : MonoBehaviour
    {

        [SerializeField] private Transform stairStart;

        [SerializeField] private Transform stairEnd;

        private Vector3 straightForward;

        [SerializeField] [Range(0f, 30f)]private float speed = 1f;

        private Vector3 straightEndPositionXZ;

        private int flagNum = 0;

        [SerializeField] private int num = 1;

        private Animator anim;

        private Vector3 preAngle;

        private float angleplusY = 0f;

        [SerializeField] [Range(0f, 50f)] private float rotateSpeed = 15f;

        [SerializeField] private float rotateAngleY = 180f;


        // Start is called before the first frame update
        void Start()
        {
            straightForward = stairEnd.position - stairStart.position;
            straightForward.Normalize();

            straightEndPositionXZ = Vector3.Scale(stairEnd.position, new Vector3(1, 0, 1));

            anim = GetComponent<Animator>();

            preAngle = this.transform.localEulerAngles;
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            FlagCheck();
            // UpdateAnimator();
        }

        void Move()
        {
            if (flagNum == 0)
            {
                StraightMove();
            }
            else if (flagNum == 1)
            {
                //Rotate();
            }
        }

        void StraightMove()
        {
            transform.position += straightForward * Time.deltaTime * speed;
        }

        void Rotate()
        {
            angleplusY += Time.deltaTime * rotateSpeed;
            //Debug.Log(angleplusY);
            this.transform.localEulerAngles = preAngle - new Vector3(0, angleplusY, 0);
        }

        void FlagCheck() 
        {
            if ((Vector3.Scale(transform.position, new Vector3(1, 0, 1)) - straightEndPositionXZ).magnitude < 1f && flagNum == 0)
            {
                flagNum = num;
                //anim.speed = 0f;
            }
            else if (angleplusY > rotateAngleY && flagNum == 1)
            {
                angleplusY = rotateAngleY;
                this.transform.localEulerAngles = preAngle - new Vector3(0, angleplusY, 0);
                flagNum = num;
                //anim.speed = 1f;
            }
        }

        /*
        void UpdateAnimator()
        {
            if (flagNum != 1) anim.SetInteger("num", flagNum);
        }
        */
    }
}
