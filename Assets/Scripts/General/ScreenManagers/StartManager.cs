﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace General
{
    public class StartManager : ScreenManager<StartManager>
    {
        private const string BGM_NAME = "Sunrise";

        [SerializeField] private Slider bgmSlider = null;
        [SerializeField] private Slider seSlider = null;
        [SerializeField] private Toggle invertAxisXToggle = null;
        [SerializeField] private Toggle invertAxisYToggle = null;
        [SerializeField] private Slider sensitivitySlider = null;

        protected override void Start()
        {
            base.Start();

            if (gfc.VMode != ViewMode.Title)
            {
                gfc.VMode = ViewMode.Title;
            }
            if (gfc.SMode != ScreenMode.Start)
            {
                gfc.SMode = ScreenMode.Start;
            }

            if (pb.BGMSlider == null)
            {
                pb.BGMSlider = bgmSlider;
                pb.SESlider = seSlider;
                pb.InvertAxisXToggle = invertAxisXToggle;
                pb.InvertAxisYToggle = invertAxisYToggle;
                pb.SensitivitySlider = sensitivitySlider;

                am.Listen();
                im.Listen();
            }

            pb.BGMSlider.value = pb.BGMVolume * 5;
            pb.SESlider.value = pb.SEVolume * 5;
            pb.InvertAxisXToggle.isOn = pb.InvertAxisX;
            pb.InvertAxisYToggle.isOn = pb.InvertAxisY;
            pb.SensitivitySlider.value = pb.Sensitivity;

            gfc.Views = views;
            am.Play(BGM_NAME);
        }

        //private void OnEnable()
        //{
        //    pb.BGMSlider.value = pb.BGMVolume * 5;
        //    pb.SESlider.value = pb.SEVolume * 5;
        //    pb.InvertAxisXToggle.isOn = pb.InvertAxisX;
        //    pb.InvertAxisYToggle.isOn = pb.InvertAxisY;
        //    pb.SensitivitySlider.value = pb.Sensitivity * 2;
        //}

    }
}