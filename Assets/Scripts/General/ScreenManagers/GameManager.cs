﻿using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine;
using System.Collections;

namespace General {
    
    public class GameManager : ScreenManager<GameManager>
    {
        // Excellentになるための境界値
        private const int EXCELLENT_TEMP = 20;
        private const int EXCELLENT_GAS = 80;
        // Niceになるための境界値
        private const int NICE_TEMP = 70;
        private const int NICE_GAS = 30;

        private const string BGM_NAME = "たぬきちの冒険";
        private const string CHASE_NAME = "クモ怪人";
        private bool started = false;
        private bool chased = false;

        [SerializeField] private Slider bgmSlider = null;
        [SerializeField] private Slider seSlider = null;
        [SerializeField] private Toggle invertAxisXToggle = null;
        [SerializeField] private Toggle invertAxisYToggle = null;
        [SerializeField] private Slider sensitivitySlider = null;


        protected override void Start()
        {
            base.Start();
            started = true;

            if (gfc.VMode != ViewMode.GameEntry)
            {
                gfc.VMode = ViewMode.GameEntry;
            }
            if (gfc.SMode != ScreenMode.Game)
            {
                gfc.SMode = ScreenMode.Game;
            }

            gfc.Views = views;
            gfc.dispatch(Signal.Forward);

            if (pb.BGMSlider == null)
            {
                pb.BGMSlider = bgmSlider;
                pb.SESlider = seSlider;
                pb.InvertAxisXToggle = invertAxisXToggle;
                pb.InvertAxisYToggle = invertAxisYToggle;
                pb.SensitivitySlider = sensitivitySlider;

                am.Listen();
                im.Listen();
            }

            pb.BGMSlider.value = pb.BGMVolume * 5;
            pb.SESlider.value = pb.SEVolume * 5;
            pb.InvertAxisXToggle.isOn = pb.InvertAxisX;
            pb.InvertAxisYToggle.isOn = pb.InvertAxisY;
            pb.SensitivitySlider.value = pb.Sensitivity;

            InitGame();
        }

        private void OnEnable()
        {
            if (started)
            {
                InitGame();
            }
        }

        protected override void Update()
        {
            if (pb.StopTheWorld)
            {
                Time.timeScale = 0f;
            }

            // TODO: 要修正，コルーチンで書き直す
            //if (!pb.IsOver)
            //{
            //    pb.Elapsed += Time.deltaTime;
            //    // pb.FrameCount ++;
            //}

            if (!chased && pb.IsChase)
            {
                chased = true;
                am.Play(CHASE_NAME);
            }

            if (chased && !pb.IsChase)
            {
                chased = false;
                am.Play(BGM_NAME);
            }

            if (pb.Elapsed > ParamBridge.LIMIT_ELAPSED)
            {
                EndGame();
            }

        }

        public void InitGame()
        {
            pb.IsOver = false;
            pb.Catched = false;
            pb.Reached = false;
            pb.Elapsed = ParamBridge.INIT_ELAPSED;
            pb.Temp = ParamBridge.TEMP_MIN;
            pb.Gas = ParamBridge.GAS_MAX;
            pb.IsCoolTime = false;
            pb.IsVoid = false;
            pb.IsCoolTime = false;
            pb.IsChase = false;
            pb.IsCrouch = false;
            pb.CountIntoVoid = 5;
            pb.IsDead = false;
            //pb.BGMSlider.value = pb.BGMVolume * 5;
            //pb.SESlider.value = pb.SEVolume * 5;
            //pb.InvertAxisXToggle.isOn = pb.InvertAxisX;
            //pb.InvertAxisYToggle.isOn = pb.InvertAxisY;
            //pb.SensitivitySlider.value = pb.Sensitivity * 2;

            am.Play(BGM_NAME);
            gfc.timer = PlayTimer();
            StartCoroutine(gfc.timer);
        }

        public void EndGame()
        {
            if (pb.IsOver)
            {
                return;
            }

            // リザルト集計
            if (pb.Catched || pb.IsDead || !pb.Reached)
            {
                // スコアを保存，ハイスコア更新
                pb.HighScore = gfc.result = Result.Lost;
            }
            // スコア: Bad
            else if (pb.Temp > NICE_TEMP || pb.Gas < NICE_GAS)
            {
                // スコアを保存，ハイスコア更新
                pb.HighScore = gfc.result = Result.Bad;
            }
            // スコア: Nice
            else if (pb.Temp > EXCELLENT_TEMP || pb.Gas < EXCELLENT_GAS)
            {
                // スコアを保存，ハイスコア更新
                pb.HighScore = gfc.result = Result.Nice;
            }
            // スコア: Excellent
            else
            {
                // スコアを保存，ハイスコア更新
                pb.HighScore = gfc.result = Result.Excellent;
            }

            gfc.dispatch(Signal.Forward);
            gfc.timer = null;
            Debug.Log("Game is over");
        }

        public IEnumerator PlayTimer()
        {
            while (!pb.IsOver)
            {
                pb.Elapsed += Time.deltaTime;
                yield return null;
            }
        }

        // 開発ビルド or エディター
#if DEBUG
        private void OnGUI()
        {
            Vector2 guiScreenSize = new Vector2(640, 360);	// 基準とする解像度
            GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / guiScreenSize.x, Screen.height / guiScreenSize.y), Vector2.zero);
            GUI.color = Color.black;
            GUILayout.BeginArea(new Rect(50, 0, 300, 250));
            GUILayout.Label($"LeftStick: H={im.AxisMove.x:F4}, V={im.AxisMove.y:F4}");
            GUILayout.Label($"RightStick: H={im.AxisLook.x:F4}, V={im.AxisLook.y:F4}");
            GUILayout.Label($"ButtonFront: {im.ButtonFront}");
            GUILayout.Label($"ButtonCrouch: {im.ButtonCrouch}");
            GUILayout.Label($"ButtonVoid: {im.ButtonVoid}");
            GUILayout.EndArea();
            //GUILayout.BeginArea(new Rect(300, 0, 300, 250));
            //GUILayout.Label($"Phase: {context.phase}");
            //GUILayout.Label($"Time: {context.time}");
            //GUILayout.Label($"Control: {context.control}");
            //GUILayout.Label($"Value: {context.ReadValue<Vector2>()}");
            //GUILayout.Label($"Interaction: {context.interaction}");
            //GUILayout.EndArea();

        }
#endif

    }

}