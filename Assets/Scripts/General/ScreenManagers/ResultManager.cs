﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace General
{
    public class ResultManager : ScreenManager<ResultManager>
    {
        private const string FANFARE_NAME = "Sunrise";
        [SerializeField] private Result which;

        protected override void Start()
        {
            base.Start();

            if (gfc.VMode != ViewMode.ResultMovie)
            {
                gfc.VMode = ViewMode.ResultMovie;
            }
            if (gfc.SMode < ScreenMode.BadResult || ScreenMode.ExcellentResult < gfc.SMode)
            {
                switch (which)
                {
                    case Result.Bad:
                        gfc.SMode = ScreenMode.BadResult;
                        break;
                    case Result.Nice:
                        gfc.SMode = ScreenMode.NiceResult;
                        break;
                    case Result.Excellent:
                        gfc.SMode = ScreenMode.ExcellentResult;
                        break;
                    default:
                        gfc.SMode = ScreenMode.BadResult;
                        break;
                }
            }

            gfc.Views = views;
            am.Play(FANFARE_NAME);
            //StartCoroutine(CountDown());
        }


        IEnumerator CountDown()
        {
            yield return new WaitForSeconds(20f);

            Debug.Log("Result is shown!");
            gfc.dispatch(Signal.Forward);
        }

    }

}
