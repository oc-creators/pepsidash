﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace General
{
    public class OpeningManager : ScreenManager<OpeningManager>
    {
        [SerializeField] private Text count;
        protected override void Start()
        {
            base.Start();

            if (gfc.VMode != ViewMode.OpeningMovie)
            {
                gfc.VMode = ViewMode.OpeningMovie;
            }
            if (gfc.SMode != ScreenMode.Opening)
            {
                gfc.SMode = ScreenMode.Opening;
            }

            gfc.Views = views;
            //StartCoroutine(CountDown());
        }


        IEnumerator CountDown()
        {
            for (int i = 0; i < 8; i++)
            {
                count.text = $"{8-i}";
                yield return new WaitForSeconds(1f);
            }

            Debug.Log("Game Start!");
            gfc.dispatch(Signal.Forward);
        }
    }

}
