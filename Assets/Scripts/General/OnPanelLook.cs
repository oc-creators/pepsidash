﻿using UnityEngine;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
#if UNITY_EDITOR
using UnityEngine.EventSystems;
#elif UNITY_ANDROID
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
#endif

namespace General
{
    public class OnPanelLook : OnScreenControl
#if UNITY_EDITOR
        , IPointerDownHandler, IPointerUpHandler, IDragHandler
#elif UNITY_ANDROID
        , TouchController.IBeganHandler, TouchController.IMovedHandler, TouchController.IEndedHandler
#endif
    {
        [InputControl(layout = "Vector2")]
        [SerializeField]
        private string m_ControlPath;

        [SerializeField]
        private TouchController con;

        private Vector2 m_PointerDownPos;

        protected override string controlPathInternal
        {
            get => m_ControlPath;
            set => m_ControlPath = value;
        }

        [SerializeField]
        private float scale = 50;

        private RectTransform rect;

#if UNITY_EDITOR
        public void OnPointerDown(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), eventData.position, eventData.pressEventCamera, out m_PointerDownPos);
        }

        public void OnDrag(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), eventData.position, eventData.pressEventCamera, out var position);
            var delta = eventData.delta;

            //delta = Vector2.ClampMagnitude(delta, scale);

            var newPos = delta / scale;
            SendValueToControl(newPos);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SendValueToControl(Vector2.zero);
        }

#elif UNITY_ANDROID
        public void OnBegan(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), touch.startScreenPosition, null, out m_PointerDownPos);
        }

        public void OnMoved(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponentInParent<RectTransform>(), touch.screenPosition, null, out var position);
            var delta = touch.delta;

            delta = Vector2.ClampMagnitude(delta, scale);

            var newPos = delta / scale;
            con.m_Look = newPos;
        }

        public void OnEnded(Touch touch)
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rect, touch.startScreenPosition))
            {
                return;
            }

            con.m_Look = Vector2.zero;
        }
#endif


        private void Start()
        {
            rect = GetComponent<RectTransform>();

#if UNITY_ANDROID && !UNITY_EDITOR
            con.began += OnBegan;
            con.moved += OnMoved;
            con.stationary += OnEnded;
            con.ended += OnEnded;
#endif
        }

#if DEBUG
        //private void OnGUI()
        //{
        //    Vector2 guiScreenSize = new Vector2(640, 360);	// 基準とする解像度
        //    GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / guiScreenSize.x, Screen.height / guiScreenSize.y), Vector2.zero);
        //    GUI.color = Color.black;
        //    GUILayout.BeginArea(new Rect(300, 0, 250, 250));

        //    GUILayout.Label($"Look_Phase: {lookContext?.phase}");
        //    GUILayout.Label($"Look_Delta: {lookContext?.ReadValue<Vector2>()}");
        //    GUILayout.Label($"Look_Control: {lookContext?.control}");

        //    GUILayout.EndArea();
        //}
#endif
    }

}
