﻿using UnityEngine;
using UnityEngine.UI;

namespace General
{
    public class ParamBridge : SingletonMonoBehaviour<ParamBridge>
    {
        protected override bool dontDestroyOnLoad { get { return true; } }

        // ==================================
        // 固定値
        // ==================================

        // 制限時間初期値上限値
        public const float INIT_ELAPSED = 0f;
        public const float LIMIT_ELAPSED = 600f;
        // 炭酸MinMax値
        public const int GAS_MIN = 0;
        public const int GAS_MAX = 100;
        // 温度MinMax値
        public const int TEMP_MIN = 0;
        public const int TEMP_MAX = 100;
        // フレーム数初期値上限値
        public const int INIT_FRAME = 0;
        public const int LIMIT_FRAME = 1000000;
        // カメラX軸反転初期値
        public const bool INIT_INVERT_AXIS_X = false;
        public const bool INIT_INVERT_AXIS_Y = false;
        // カメラ感度初期値
        public const int INIT_SENSITIVITY = 10;

        // ==================================
        // パラメータ変数
        // ==================================

        // 時を止めたかどうか
        [SerializeField] private bool stopTheWorld = false;
        public bool StopTheWorld
        {
            get { return stopTheWorld; }
            set
            {
                stopTheWorld = value;
                if (value)
                {
                    Time.timeScale = 1f;
                }
            }
        }
        // 経過時間
        [SerializeField] private float elapsed = INIT_ELAPSED;
        public float Elapsed
        {
            get { return elapsed; }
            set { elapsed = value; }
        }
        // ゲーム終了したかどうか
        [SerializeField] private bool isOver = false;
        public bool IsOver
        {
            get { return isOver; }
            set { isOver = value; }
        }
        // 警備員に捕まったかどうか
        [SerializeField] private bool catched = false;
        public bool Catched
        {
            get { return catched; }
            set { catched = value; }
        }
        // ゴールに到達したかどうか
        [SerializeField] private bool reached = false;
        public bool Reached
        {
            get { return reached; }
            set { reached = value; }
        }
        // 炭酸値
        [SerializeField] private int gas = GAS_MAX;
        public int Gas
        {
            get { return gas; }
            set { gas = value; }
        }
        // 温度値
        [SerializeField] private int temp = TEMP_MIN;
        public int Temp
        {
            get { return temp; }
            set { temp = value; }
        }
        // フレーム数
        private int frameCount = INIT_FRAME;
        public int FrameCount
        {
            get { return frameCount; }
            set { frameCount = value; }
        }
        // 虚空判定
        [SerializeField] private bool isVoid = false;
        public bool IsVoid
        {
            get { return isVoid; }
            set { isVoid = value; }
        }
        // クールタイム(trueのときがクールタイム中)
        [SerializeField] private bool isCoolTime = false;
        public bool IsCoolTime
        {
            get { return isCoolTime; }
            set { isCoolTime = value; }
        }
        // 追いかけられているかどうか
        [SerializeField] private bool isChase = false;
        public bool IsChase
        {
            get { return isChase; }
            set { isChase = value; }
        }
        // しゃがんでいるかどうか
        [SerializeField] private bool isCrouch = false;
        public bool IsCrouch
        {
            get { return isCrouch; }
            set { isCrouch = value; }
        }
        // 虚空回数制限
        [SerializeField] private int countIntoVoid = 5;
        public int CountIntoVoid
        {
            get { return countIntoVoid; }
            set { countIntoVoid = value; }
        }
        // 小さくなる（パルプンテ）
        [SerializeField] private bool isShrink = false;
        public bool IsShrink
        {
            get { return isShrink; }
            set { isShrink = value; }
        }
        [SerializeField] private bool isDead = false;
        public bool IsDead
        {
            get { return isDead; }
            set { isDead = value; }
        }
        [SerializeField] private bool isGrounded = true;
        public bool IsGrounded
        {
            get { return isGrounded; }
            set { isGrounded = value; }
        }
        
        // ==========================================
        // Jsonに保存するパラメータ
        // ==========================================

        // ハイスコア
        [SerializeField] private Result highScore = Result.None;
        public Result HighScore
        {
            get { return highScore; }
            set 
            { 
                if (value > highScore)
                {
                    highScore = value;
                }
            } 
        }

        // BGM音量
        [SerializeField] private float bgmVolume = 1f;
        public float BGMVolume
        {
            get { return bgmVolume; }
            set { bgmVolume = value; }
        }

        // SE音量
        [SerializeField] private float seVolume = 1f;
        public float SEVolume
        {
            get { return seVolume; }
            set { seVolume = value; }
        }

        // カメラ反転(左右) 反転するときtrue
        [SerializeField] private bool invertAxisY = INIT_INVERT_AXIS_Y;
        public bool InvertAxisY
        {
            get { return invertAxisY; }
            set { invertAxisY = value; }
        }

        // カメラ反転(上下) 反転するときtrue
        [SerializeField] private bool invertAxisX = INIT_INVERT_AXIS_X;
        public bool InvertAxisX
        {
            get { return invertAxisX; }
            set { invertAxisX = value; }
        }

        // カメラ感度
        [SerializeField] private int sensitivity = INIT_SENSITIVITY;
        public int Sensitivity
        {
            get { return sensitivity; }
            set { sensitivity = value; }
        }


        [SerializeField] private string version = "0.0.0";
        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        [SerializeField] private string buildAt = "yyyyMMdd-HHmm";
        public string BuildAt
        {
            get { return buildAt; }
            set { buildAt = value; }
        }

        [SerializeField] private bool first_run = true;
        public bool FirstRun
        {
            get { return first_run; }
            set { first_run = value; }
        }

        // ==========================================
        // スライダー、トグル等
        // ==========================================
        [SerializeField] private Slider bgmSlider = null;
        public Slider BGMSlider
        {
            get => bgmSlider;
            set => bgmSlider = value;
        }

        [SerializeField] private Slider seSlider = null;
        public Slider SESlider
        {
            get => seSlider;
            set => seSlider = value;
        }

        [SerializeField] private Toggle invertAxisXToggle = null;
        public Toggle InvertAxisXToggle
        {
            get => invertAxisXToggle;
            set => invertAxisXToggle = value;
        }

        [SerializeField] private Toggle invertAxisYToggle = null;
        public Toggle InvertAxisYToggle
        {
            get => invertAxisYToggle;
            set => invertAxisYToggle = value;
        }

        [SerializeField] private Slider sensitivitySlider = null;
        public Slider SensitivitySlider
        {
            get => sensitivitySlider;
            set => sensitivitySlider = value;
        }

        protected override void Awake()
        {
            if (CheckInstance())
            {
                var param_json = $"{Application.persistentDataPath}/Data/param.json";
                Debug.Log($"Import {param_json}");
                var param = new Param();
                JsonManager.Load(ref param, param_json);
                highScore = param.high_score;
                bgmVolume = param.bgm_volume;
                seVolume = param.se_volume;
                invertAxisX = param.invert_axis_x;
                invertAxisY = param.invert_axis_y;
                sensitivity = param.sensitivity;
                first_run = param.first_run;

                if (first_run)
                {
                    var info = new Info();
                    var json_str = Resources.LoadAll<TextAsset>("Data")[0].ToString();
                    JsonUtility.FromJsonOverwrite(json_str, info);
                    version = info.version;
                    buildAt = info.buildAt;
                    Debug.Log($"version: {version}, buildAt: {buildAt}");
                
                    var info_json2 = $"{Application.persistentDataPath}/Data/info.json";
                    Debug.Log($"Clone to {info_json2}");
                    JsonManager.Dump(ref info, info_json2);
                }
                else
                {
                    var info_json = $"{Application.persistentDataPath}/Data/info.json";
                    Debug.Log($"Import {info_json}");
                    var info = new Info();
                    JsonManager.Load(ref info, info_json);
                    version = info.version;
                    buildAt = info.buildAt;
                    Debug.Log($"version: {version}, buildAt: {buildAt}");
                }

                // スライダー、トグル初期化
            }
        }

#if UNITY_EDITOR
        private void OnApplicationQuit()
        {
            var param = new Param();
            param.high_score = highScore;
            param.bgm_volume = bgmVolume;
            param.se_volume = seVolume;
            param.invert_axis_x = invertAxisX;
            param.invert_axis_y = invertAxisY;
            param.sensitivity = sensitivity;
            param.unlock = 0;
            //if (first_run)
            //{
            //    param.first_run = true;
            //}

            var param_json = $"{Application.persistentDataPath}/Data/param.json";
            Debug.Log($"Quit: Export {param_json}");
            JsonManager.Dump(ref param, param_json);
        }
#else
        private void OnApplicationPause(bool pause)
        {
            if (!pause) return;
          
            var param = new Param();
            param.high_score = highScore;
            param.bgm_volume = bgmVolume;
            param.se_volume = seVolume;
            param.unlock = 0;
            //if (first_run)
            //{
            //    param.first_run = false;
            //}

            var param_json = $"{Application.persistentDataPath}/Data/param.json";
            JsonManager.Dump(ref param, param_json);
            Debug.Log($"Pause: Export {param_json}");
        }
#endif
    }
}
