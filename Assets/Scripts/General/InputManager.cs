﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

namespace General
{
    public class InputManager : SingletonMonoBehaviour<InputManager>
    {
        protected override bool dontDestroyOnLoad { get { return false; } }
        private Vector2 axisMove = default, axisLook = default;
        private bool buttonFront = default, buttonCrouch = default, buttonVoid = default, buttonDash = default, buttonCancel = default;

        private int invX = 1, invY = 1;
        [SerializeField]
        private int sensi = 5;

        private MyInputAction inputAction;
        private TouchController con;
        private ParamBridge pb;

        public Vector2 AxisMove { get => axisMove; set => axisMove = value; }
        public Vector2 AxisLook { get => axisLook; set => axisLook = value; }
        public bool ButtonFront { get => buttonFront; set => buttonFront = value; }
        public bool ButtonCrouch { get => buttonCrouch; set => buttonCrouch = value; }
        public bool ButtonVoid { get => buttonVoid; set => buttonVoid = value; }
        public bool ButtonDash { get => buttonDash; set => buttonDash = value; }
        public bool ButtonCancel { get => buttonCancel; set => buttonCancel = value; }

        protected override void Init()
        {
            inputAction = new MyInputAction();
            con = GetComponent<TouchController>();
            pb = ParamBridge.Instance;

            invX = pb.InvertAxisX ? -1 : 1;
            invY = pb.InvertAxisY ? -1 : 1;
            sensi = pb.Sensitivity;

            //ゲーム上の設定と紐づけする
            Listen();
        }

        private void OnEnable()
        {
            inputAction.Enable();
        }

        private void OnDisable()
        {
            inputAction.Disable();
        }

        protected override void Update()
        {
#if UNITY_EDITOR
            // キーボード・マウスからの入力
            axisLook = inputAction.Player.Look.ReadValue<Vector2>();   // +: right arrow, -: left arrow
            axisMove = inputAction.Player.Move.ReadValue<Vector2>();       // +: d key, -: a key
            buttonFront = inputAction.Player.LookAtFront.triggered;                // r key
            buttonCrouch = inputAction.Player.Crouch.triggered;              // left ctrl key, right ctrl key
            buttonVoid = inputAction.Player.EnterToVoid.triggered;                  // v key
            buttonDash = inputAction.Player.Dash.triggered;                  // left shift key, right shift key

            //Debug.Log(inputAction.Player.MousePosition.ReadValue<Vector2>());
#elif UNITY_ANDROID
            // UIからの入力
            axisLook.x = con.m_Look.x;   // +: right arrow, -: left arrow
            axisLook.y = 0f;
            axisMove = con.m_Move;       // +: d key, -: a key
            buttonFront = con.m_Front;                // r key
            buttonCrouch = con.m_Crouch;              // left ctrl key, right ctrl key
            buttonVoid = con.m_Void;                  // v key
            buttonDash = con.m_Dash;                  // left shift key, right shift key
#endif
            axisLook *= new Vector2(invX * sensi, invY * sensi);
        }

        public static Rect UItoScreenRect(RectTransform rt)
        {
            var rateX = Screen.safeArea.width / rt.parent.transform.GetComponentInParent<RectTransform>().sizeDelta.x;
            var rateY = Screen.safeArea.height / rt.parent.transform.GetComponentInParent<RectTransform>().sizeDelta.y;
            var x = rt.position.x;
            var y = rt.position.y;
            var w = rt.rect.width * rt.localScale.x * rateX;
            var h = rt.rect.height * rt.localScale.y * rateY;

            return new Rect(x, y, w, h);
        }

        public void ReflectChanges()
        {
            pb.InvertAxisX = invX == -1 ? true : false;
            pb.InvertAxisY = invY == -1 ? true : false;
            pb.Sensitivity = sensi;
        }

        public void Listen()
        {
            pb.InvertAxisXToggle?.onValueChanged.AddListener(value => invX = value ? -1 : 1);
            pb.InvertAxisYToggle?.onValueChanged.AddListener(value => invY = value ? -1 : 1);
            pb.SensitivitySlider?.onValueChanged.AddListener(value => sensi = (int)value);
        }
    }
}
