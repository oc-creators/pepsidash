﻿using System.Text;
using System.IO;
using UnityEngine;

namespace General
{
    public class JsonManager
    {
        public static void Check(string path)
        {
            var dirname = Path.GetDirectoryName(path);
            if (!Directory.Exists(dirname))
            {
                Debug.Log($"NOT Existed: Create {dirname}");
                Directory.CreateDirectory(dirname);
            }
        }

        public static string Import(string path)
        {
            Debug.Assert(!path.Contains("://"));
            string json;
            using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var sr = new StreamReader(fs, Encoding.UTF8))
                {
                    json = sr.ReadToEnd();
                }
            }

            return json;
        }

        public static void Export(string json_str, string path)
        {
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                using (var sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    sw.WriteLine(json_str);
                } 
            }
        }

        public static void Load<T>(ref T obj, string path)
        {
            Check(path);
            var json = Import(path);
            JsonUtility.FromJsonOverwrite(json, obj);
        }

        public static void Dump<T>(ref T obj, string path)
        {
            var json_str = JsonUtility.ToJson(obj, true);
            Export(json_str, path);
        }
    }
}