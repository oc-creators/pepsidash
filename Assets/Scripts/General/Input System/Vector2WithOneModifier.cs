using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.InputSystem.Editor;
#endif

#if UNITY_EDITOR
[InitializeOnLoad]
#endif

[DisplayStringFormat("{vector}*{modifier}")]
public class Vector2WithOneModifier : InputBindingComposite<Vector2>
{
    #if UNITY_EDITOR
    static Vector2WithOneModifier()
    {
        Initialize();
    }

    #endif

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        InputSystem.RegisterBindingComposite<Vector2WithOneModifier>();
    }

    [InputControl(layout = "Vector2")]
    public int modifier;

    [InputControl(layout = "Vector2")]
    public int vector;

    public int x;
    public int y;
    public int w;
    public int h;
    private Rect region;

    public override Vector2 ReadValue(ref InputBindingCompositeContext context)
    {
        Debug.Assert(w != 0 && h != 0);
        var region = new Rect(x, y, w, h);
        var pos = context.ReadValue<Vector2, Vector2MagnitudeComparer>(modifier);
        var contain = region.Contains(pos);
        if (contain)
        {
            return context.ReadValue<Vector2, Vector2MagnitudeComparer>(vector);
        }
        return default;
    }
}