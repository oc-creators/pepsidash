﻿using System;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

namespace General
{
    public class TouchController : MonoBehaviour
    {
        public Action<Touch> began;
        public Action<Touch> canceled;
        public Action<Touch> ended;
        public Action<Touch> moved;
        public Action<Touch> stationary;
        public Action<Touch> none;

        public Vector2 m_Move;
        public Vector2 m_Look;
        public bool m_Front;
        public bool m_Crouch;
        public bool m_Void;
        public bool m_Dash;
        public bool m_Jump;

        private void OnEnable()
        {
            EnhancedTouchSupport.Enable();
        }

        private void OnDisable()
        {
            EnhancedTouchSupport.Disable();
        }

        private void Update()
        {            
            if (m_Front)
            {
                m_Front = !m_Front;
            }

            if (m_Crouch)
            {
                m_Crouch = !m_Crouch;
            }

            if (m_Void)
            {
                m_Void = !m_Void;
            }

            foreach (var touch in Touch.activeTouches)
            {
                Debug.Log(touch.tapCount);

                //none?.Invoke(touch);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        began?.Invoke(touch);
                        break;

                    case TouchPhase.Canceled:
                        canceled?.Invoke(touch);
                        break;

                    case TouchPhase.Ended:
                        ended?.Invoke(touch);
                        break;

                    case TouchPhase.Moved:
                        moved?.Invoke(touch);
                        break;

                    case TouchPhase.Stationary:
                        stationary?.Invoke(touch);
                        break;

                    default:
                        break;
                }
            }

        }

        private void OnGUI()
        {
            //GUI.DrawTexture(regionTouchScreen, _texture, ScaleMode.StretchToFill, true, 0, Color.white, 3, 0);
            //GUI.DrawTexture(regionVoidButton, _texture, ScaleMode.StretchToFill, true, 0, Color.white, 3, 0);
            //GUI.DrawTexture(regionCrouchButton, _texture, ScaleMode.StretchToFill, true, 0, Color.white, 3, 0);
            //GUI.DrawTexture(regionLeftStick, _texture, ScaleMode.StretchToFill, true, 0, Color.white, 3, 0);
        }

        public interface IBeganHandler
        {
            void OnBegan(Touch touch);
        }

        public interface ICanceledHandler
        {
            void OnCanceled(Touch touch);
        }

        public interface IMovedHandler
        {
            void OnMoved(Touch touch);
        }

        public interface IEndedHandler
        {
            void OnEnded(Touch touch);
        }

        public interface IStationaryHandler
        {
            void OnStationary(Touch touch);
        }

        public interface INoneHandler
        {
            void OnNone(Touch touch);
        }
    }
}
