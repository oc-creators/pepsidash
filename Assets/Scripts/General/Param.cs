﻿using System;
using UnityEngine;

namespace General {
    [Serializable]
    public class Param
    {
        [SerializeField]
        public float bgm_volume = 1f;
        [SerializeField]
        public float se_volume = 1f;
        [SerializeField]
        public bool invert_axis_x = false;
        [SerializeField]
        public bool invert_axis_y = false;
        [SerializeField]
        public int sensitivity = 5;
        [SerializeField]
        public Result high_score = Result.None;
        [SerializeField]
        public int unlock = 0;
        [SerializeField]
        public bool first_run = true;
    }

    public class Info
    {
        [SerializeField]
        public string version = "0.0.0";
        [SerializeField]
        public string buildAt = "yyyyMMdd-HHmm";
    }
}