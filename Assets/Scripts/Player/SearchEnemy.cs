﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player{
    public class SearchEnemy : MonoBehaviour
    {
        private List<MoveEnemy> SearchingList = new List<MoveEnemy>();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            SearchChaseEnemy();
        }

        void SearchChaseEnemy()
        {
            foreach(MoveEnemy me in SearchingList)
            {
                if (me.getState() == "chase")
                {
                    ParamBridge.Instance.IsChase = true;
                    return;
                }
            }
            ParamBridge.Instance.IsChase = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                SearchingList.Add(other.GetComponent<MoveEnemy>());
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                SearchingList.Remove(other.GetComponent<MoveEnemy>());
            }
        }

    }
}
