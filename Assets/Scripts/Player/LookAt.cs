﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class LookAt : MonoBehaviour
    {
        [SerializeField] private Transform target;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Look();
        }

        void Look()
        {
            if (target == null) return;
            this.transform.forward = this.transform.position - target.position;// なんか知らんけど逆
        }

    }
}
