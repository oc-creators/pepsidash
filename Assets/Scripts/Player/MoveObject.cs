﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player
{
    public class MoveObject : MonoBehaviour
    {
        private Rigidbody rb;

        [SerializeField] [Range(0f, 50f)] private float forceRate = 5f;
        [SerializeField] [Range(0f, 50f)] private float upRate = 1f;

        // Start is called before the first frame update
        void Start()
        {
            rb = this.GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                // Rigidbody prb = collision.gameObject.GetComponent<Rigidbody>();
                // Vector3 direction = prb.velocity;
                // Debug.Log("Velocity :"+ direction + ", Magnitude :"+ direction.magnitude);
                /*
                // 初版
                direction *= forceRate;
                direction += new Vector3(0, 1, 0) * upRate;
                Debug.Log("Direction :"+ direction);
                rb.AddForce(direction, ForceMode.Impulse);

                // Playerのdirectionで飛ばす方向を決める
                if (direction.magnitude > 0f)
                {
                    direction.Normalize();
                    direction *= -1;
                    Debug.Log("Direction :" + direction + ", Forward :" + collision.gameObject.transform.forward.normalized);
                    direction += new Vector3(0, 1, 0);
                    rb.AddForce(direction * forceRate, ForceMode.Impulse);
                }
                */
                // Playerのforwardで飛ばす方向を決める
                Vector3 forward = collision.transform.forward;
                forward.Normalize();
                //forward *= -1;
                // Debug.Log("Direction :" + direction + ", Forward :" + collision.gameObject.transform.forward.normalized);
                forward += new Vector3(0, 1, 0);
                ChangeForceRate();
                rb.AddForce(forward * forceRate, ForceMode.Impulse);
            }
        }

        private void ChangeForceRate()
        {
            if (ParamBridge.Instance.IsVoid)
            {
                forceRate = 10f;
            }
            else if (ParamBridge.Instance.IsCrouch)
            {
                forceRate = 0f;
            }
            else
            {
                forceRate = 5f;
            }
        }

    }
}
