﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class IKTest : MonoBehaviour
    {
        [SerializeField] private Transform handAnchorR = null;
        [SerializeField] private Transform handAnchorL = null;
        [SerializeField] private Transform footAnchorR = null;
        [SerializeField] private Transform footAnchorL = null;



        private Animator animator;

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnAnimatorIK()
        {
            //右手
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            animator.SetIKPosition(AvatarIKGoal.RightHand, handAnchorR.position);
            animator.SetIKRotation(AvatarIKGoal.RightHand, handAnchorR.rotation);

            // 左手
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
            animator.SetIKPosition(AvatarIKGoal.LeftHand, handAnchorL.position);
            animator.SetIKRotation(AvatarIKGoal.LeftHand, handAnchorL.rotation);

            //右足
            animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
            animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
            animator.SetIKPosition(AvatarIKGoal.RightFoot, footAnchorR.position);
            animator.SetIKRotation(AvatarIKGoal.RightFoot, footAnchorR.rotation);

            // 左足
            animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
            animator.SetIKPosition(AvatarIKGoal.LeftFoot, footAnchorL.position);
            animator.SetIKRotation(AvatarIKGoal.LeftFoot, footAnchorL.rotation);
        }
    }
}
