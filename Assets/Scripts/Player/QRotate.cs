﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class QRotate : MonoBehaviour
    {
        [SerializeField] [Range(0f, 1f)] private float xAngle = 1f;
        [SerializeField] [Range(0f, 1f)] private float yAngle = 1f;
        [SerializeField] [Range(0f, 1f)] private float zAngle = 1f;
        [SerializeField] [Range(0f, 100f)] private float rotateSpeed = 1f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Rotate();
        }

        void Rotate()
        {
            this.transform.localEulerAngles += new Vector3(xAngle, yAngle, zAngle) * rotateSpeed;
        }
    }
}
