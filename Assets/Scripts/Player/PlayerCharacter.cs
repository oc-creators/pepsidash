﻿using UnityEngine;
using General;

namespace Player
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class PlayerCharacter : MonoBehaviour
	{
		//[SerializeField] float m_MovingTurnSpeed = 360;
		//[SerializeField] float m_StationaryTurnSpeed = 180;
		//[SerializeField] float m_JumpPower = 12f;
		//[Range(1f, 4f)] [SerializeField] float m_GravityMultiplier = 2f;
		[Range(0f, 30f)] [SerializeField] float speed = 2f;
		//[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
		//[SerializeField] float m_MoveSpeedMultiplier = 1f;
		//[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;

		Rigidbody m_Rigidbody;
		Animator m_Animator;
		bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		//float m_TurnAmount;
		//float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
		bool m_Jumping;
		//[Range(0f, 600f)] public float jumpForce = 300.0f;

		public Transform center;

		[SerializeField] private Transform head;

		//[SerializeField] private bool isVoid = false;

		private bool isChange = false;

		[SerializeField] private bool isCoolTime = false;

		//[SerializeField] [Range(0, 5)] private int countIntoVoid = 2;

		[SerializeField] [Range(0f, 60f)] private float voidTime = 5f;

		private float elapsedVoidTime = 0f;

		[SerializeField] [Range(0f, 50f)] private float coolTime = 15f;

		private float elapsedCoolTime = 0f;

		[SerializeField] [Range(0f, 1f)] private float transparentAlfa = 0.2f;

		[SerializeField] [Range(0f, 10f)] private float alfaSpeed = 2f;

		[SerializeField] private SkinnedMeshRenderer surface;

		[SerializeField] private SkinnedMeshRenderer joints;

		[SerializeField] private MeshRenderer pepsis;

		private Color surfaceColor;

		private Color jointsColor;

		private Color pcanColor;

		private Color pColor;

		[SerializeField] private Material[] m_Surface;

		[SerializeField] private Material[] m_Joints;

		[SerializeField] private Material[] m_Pcans;

		[SerializeField] private Material[] m_Ps;

		[SerializeField] [Range(0f, 1f)] private float decelerateSpeedRate = 0.2f;

		[SerializeField] [Range(0f, 5f)] private float voidRunSpeedRate = 2f;

		private Vector3 firstPosition;

		private Vector3 firstForward;

		[SerializeField] private Pepsi pepsi;

		//private AudioSource audioSource;

		[SerializeField] private AudioClip sound1;

		[SerializeField] private AudioClip sound2;

		private float playerScale = 1f;

		[SerializeField] [Range(0f, 1f)] private float shrinkScale = 0.25f;

		[SerializeField] [Range(0f, 10f)] private float shrinkRate = 2f;

		[SerializeField] private float speedUpRate = 3f;


		void Start()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
			//audioSource = GetComponent<AudioSource>();
			m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;

			//m_IsGrounded = true;
			ParamBridge.Instance.IsGrounded = true;

			if (center == null) center = this.transform;

			firstPosition = this.transform.position;
			firstForward = this.transform.forward;
		}


		public void Move(Vector3 move, Vector3 camForward, Vector3 input, bool modeVoid, bool jump, bool dash, float speedRate)
		{

			// convert the world relative moveInput vector into a local-relative
			// turn amount and forward amount required to head in the desired
			// direction.
			//if (move.magnitude > 1f) move.Normalize(); 仕様変更
			//move = transform.InverseTransformDirection(move);
			CheckGroundStatus();

			//if (input.z > 0) m_Rigidbody.MovePosition(transform.position + camForward.normalized * Time.deltaTime * input.z * speed);
			//m_Rigidbody.MovePosition(transform.position + camForward.normalized * Time.deltaTime * input.z * speed);　仕様変更

			// 生きている時だけ
			if (!ParamBridge.Instance.IsDead)
			{
				// 移動
				if (ParamBridge.Instance.IsVoid)
				{
					m_Rigidbody.MovePosition(transform.position + move * Time.deltaTime * speed * voidRunSpeedRate * speedRate);
					//m_Rigidbody.MovePosition(transform.position + move.normalized * Time.deltaTime * speed * voidRunSpeedRate * speedRate);
				}
				else if (ParamBridge.Instance.IsCrouch)
				{
					m_Rigidbody.MovePosition(transform.position + move * Time.deltaTime * speed * decelerateSpeedRate * speedRate);
					//m_Rigidbody.MovePosition(transform.position + move.normalized * Time.deltaTime * speed * decelerateSpeedRate * speedRate);

				}
				else
				{
					m_Rigidbody.MovePosition(transform.position + move * Time.deltaTime * speed * speedRate);
					//m_Rigidbody.MovePosition(transform.position + move.normalized * Time.deltaTime * speed);
				}

				if (move.magnitude > 0.01f) transform.forward = Vector3.Slerp(transform.forward, move, Time.deltaTime * speed * speedRate);// 方向転換
			}

			/* ジャンプ廃止
			if (m_IsGrounded && jump)
			{
				m_Rigidbody.AddForce(Vector3.up * jumpForce);
				m_IsGrounded = false;
			}
			*/

			// 仕様変更につき
			//move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			//m_TurnAmount = Mathf.Atan2(move.x, move.z);
			//m_ForwardAmount = move.z;

			//ApplyExtraTurnRotation(input);

			//虚空システム
			IntoVoid(modeVoid);

			//ScaleCapsuleForJumping(jump);ジャンプ廃止
			ScaleCapsuleForCrouching();

			// 初期化実験
			//if (Input.GetKey(KeyCode.Space)) Init();

			// 縮むかどうか(パルプンテ)
			Shrinking();

			UpdateAnimator(move, jump, speedRate);

			pepsi.UpdateRotateAndPosition(this.transform, playerScale);
		}


		void ScaleCapsuleForJumping(bool jump)
		{
			//if (!m_IsGrounded)
			if (ParamBridge.Instance.IsGrounded)
			{
				if (m_Jumping) return;
				m_Capsule.height = m_Capsule.height * 0.9f;
				//m_Capsule.center += m_Capsule.center / 2f;
				m_Capsule.center = center.position;
				m_Jumping = true;
			}
			else
			{
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Jumping = false;
			}
		}


		void ScaleCapsuleForCrouching()
		{
			//if (m_IsGrounded && ParamBridge.Instance.IsCrouch)
			if (ParamBridge.Instance.IsGrounded && ParamBridge.Instance.IsCrouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}


		void UpdateAnimator(Vector3 move, bool jump, float speedRate)
		{
			if (ParamBridge.Instance.IsDead)
            {
				m_Animator.SetBool("Dead", true);
				m_Animator.SetBool("Ground", false);
				m_Animator.SetBool("Crouching", false);
				m_Animator.SetBool("Void", false);
				return;
            }

			//if (m_IsGrounded)
			if (ParamBridge.Instance.IsGrounded)
			{
				if (ParamBridge.Instance.IsVoid)
                {
					m_Animator.SetBool("Dead", false);
					m_Animator.SetBool("Ground", false);
					m_Animator.SetBool("Crouching", false);
					m_Animator.SetBool("Void", true);
				}
				else if (ParamBridge.Instance.IsCrouch)
				{
					m_Animator.SetBool("Dead", false);
					m_Animator.SetBool("Ground", false);
					m_Animator.SetBool("Crouching", true);
					m_Animator.SetBool("Void", false);
				}
				else
				{
					m_Animator.SetBool("Dead", false);
					m_Animator.SetBool("Ground", true);
					m_Animator.SetBool("Crouching", false);
					m_Animator.SetBool("Void", false);
				}
			}

			m_Animator.SetFloat("Speed", speedRate);


		}

		/*
		void ApplyExtraTurnRotation(Vector3 input)
		{
			if (input.z < 0) return;
			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
			//transform.Rotate(0, m_TurnAmount * Time.deltaTime * 180, 0);
		}
		*/

		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				m_GroundNormal = hitInfo.normal;
				// m_IsGrounded = true;
				ParamBridge.Instance.IsGrounded = true;
				//m_Animator.applyRootMotion = true;
			}
			else
			{
				// m_IsGrounded = false;
				ParamBridge.Instance.IsGrounded = false;
				m_GroundNormal = Vector3.up;
				//m_Animator.applyRootMotion = false;
			}
		}

		void IntoVoid(bool isButton)
		{
			// 入力受付
			if (!ParamBridge.Instance.IsVoid && isButton && ParamBridge.Instance.CountIntoVoid > 0 && !ParamBridge.Instance.IsCoolTime && !isChange)
			{
				isChange = true;
				surface.material = m_Surface[1];
				joints.material = m_Joints[1];
				pepsis.materials[0] = m_Pcans[1];
				pepsis.materials[1] = m_Ps[1];
				//Debug.Log(m_Pcans[1].name);
				//Debug.Log(m_Ps[1].name);
				AudioManager.Instance.PlaySE(sound1);
				ParamBridge.Instance.Gas -= 20;
			}

			// 虚空中の動作 それ以外も(クールタイム)
			if (ParamBridge.Instance.IsVoid)
			{
				elapsedVoidTime += Time.deltaTime;
				//Debug.Log("Player in the void.");

				if (elapsedVoidTime > voidTime)
				{
					isCoolTime = true;
					ParamBridge.Instance.IsCoolTime = isCoolTime;
					isChange = true;
					ParamBridge.Instance.CountIntoVoid--;
					ParamBridge.Instance.IsCrouch = false;
					elapsedVoidTime = 0f;
					elapsedCoolTime = 0f;
					//Debug.Log("Leave from void.");
					// プレイヤーのテクスチャーを戻す
					AudioManager.Instance.PlaySE(sound1);
				}
			}
			else
			{
				if (isCoolTime)
				{
					elapsedCoolTime += Time.deltaTime;
					if (elapsedCoolTime > coolTime)
					{
						isCoolTime = false;
						ParamBridge.Instance.IsCoolTime = isCoolTime;
					}
				}
			}

			// 変化中の動作
			if (isChange)
			{
				if (ParamBridge.Instance.IsVoid)
				{
					//透明度を上げる(元に戻る)
					surfaceColor = surface.material.color;
					jointsColor = joints.material.color;
					pcanColor = pepsis.materials[0].color;
					pColor = pepsis.materials[1].color;
					surfaceColor.a += Time.deltaTime * alfaSpeed;
					jointsColor.a += Time.deltaTime * alfaSpeed;
					pcanColor.a += Time.deltaTime * alfaSpeed;
					pColor.a += Time.deltaTime * alfaSpeed;

					if (surfaceColor.a >= 1f) // 透明度が一定値より上になったら
					{
						surfaceColor.a = 1f;
						jointsColor.a = 1f;
						pcanColor.a = 1f;
						pColor.a = 1f;
						surface.material.color = surfaceColor;
						joints.material.color = jointsColor;
						pepsis.materials[0].color = pcanColor;
						pepsis.materials[1].color = pColor;
						surface.material = m_Surface[0];
						joints.material = m_Joints[0];
						pepsis.materials[0] = m_Pcans[0];
						pepsis.materials[1] = m_Ps[0];
						//isVoid = false;
						isChange = false;
						ParamBridge.Instance.IsVoid = false;
						//AudioManager.Instance.PlaySE(sound2);//SE消す(sound2に変換)
					}
					surface.material.color = surfaceColor;
					joints.material.color = jointsColor;
					pepsis.materials[0].color = pcanColor;
					pepsis.materials[1].color = pColor;
				}
				else
				{
					//透明度を下げる(虚空に入る)
					surfaceColor = surface.material.color;
					jointsColor = joints.material.color;
					pcanColor = pepsis.materials[0].color;
					pColor = pepsis.materials[1].color;
					surfaceColor.a -= Time.deltaTime * alfaSpeed;
					jointsColor.a -= Time.deltaTime * alfaSpeed;
					pcanColor.a -= Time.deltaTime * alfaSpeed;
					pColor.a -= Time.deltaTime * alfaSpeed;

					if (surfaceColor.a < transparentAlfa) // 透明度が一定値より下になったら
					{
						surfaceColor.a = transparentAlfa;
						jointsColor.a = transparentAlfa;
						pcanColor.a = transparentAlfa;
						pColor.a = transparentAlfa;
						//isVoid = true;
						isChange = false;
						ParamBridge.Instance.IsVoid = true;
						AudioManager.Instance.PlaySE(sound2);//虚空中のSE開始(sound2に変換)
					}
					surface.material.color = surfaceColor;
					joints.material.color = jointsColor;
					pepsis.materials[0].color = pcanColor;
					pepsis.materials[1].color = pColor;
				}
			}
		}

		/*
		public bool getIsVoid()
		{
			return isVoid;
		}
		*/

		public Transform getHead()
        {
			return head;
        }

		private void Shrinking()
        {
            if (ParamBridge.Instance.IsShrink)
            {
				playerScale -= Time.deltaTime * shrinkRate;

				if(playerScale <= shrinkScale)
                {
					playerScale = shrinkScale;
					ParamBridge.Instance.IsShrink = false;
                }

				this.transform.localScale = Vector3.one * playerScale;
            }
        }

		public void speedUp()
        {
			speed *= speedUpRate;
        }

		public void Init()
        {
			transform.position = firstPosition;
			transform.forward = firstForward;
			Camera.main.transform.GetComponent<RotateAroundPlayer>().Init();
        }
	}
}
