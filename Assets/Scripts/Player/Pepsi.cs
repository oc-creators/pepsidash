﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player
{
    public class Pepsi : MonoBehaviour
    {

        private Vector3 firstUp;

        private Vector3 firstPosition;

        [SerializeField] private Transform headPosition;

        //private Vector3 headTopPosition;

        [SerializeField] private Transform rightHandPosition;

        [SerializeField] private Transform leftHandPosition;

        [SerializeField] private Transform pepsiFirstPosition;

        [SerializeField] private Vector3 offset;


        // Start is called before the first frame update
        void Start()
        {
            firstUp = transform.up;
            firstPosition = transform.position;
            pepsiFirstPosition.position = transform.position;
            pepsiFirstPosition.forward = transform.forward;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateRotateAndPosition(Transform pTrans, float pScale)
        {
            if ((rightHandPosition.position - headPosition.position).magnitude < 0.3f * pScale)
            {
                transform.forward = pTrans.forward;
                //transform.up = Vector3.up;
                //transform.position = headPosition.position;
                transform.position = (rightHandPosition.position + leftHandPosition.position) / 2f + ((offset.x * pTrans.right) + (offset.y * pTrans.up) + (offset.z * pTrans.forward)) * pScale;
            }
            else
            {
                transform.forward = pepsiFirstPosition.forward;
                transform.position = pepsiFirstPosition.position;
            }
            /*
            if (!(isVoid || crouch) && (rightHandPosition.position - headPosition.position).magnitude < 0.2f)
            {
                transform.forward = forward;
                //transform.up = Vector3.up;
                transform.position = headPosition.position;
                //transform.position = (rightHandPosition.position + leftHandPosition.position) / 2f + offset;
            }
            else if (isVoid && (rightHandPosition.position - headPosition.position).magnitude < 0.2f)
            {
                
            }
            else
            {
                //transform.up = firstUp;
                transform.position = firstPosition;
            }
            */
        }
    }
}
