﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player
{
    public class Parupunte : MonoBehaviour
    {
        [SerializeField] private GameObject particle1;

        [SerializeField] private GameObject particle2;

        [SerializeField] private bool debugRandom = false;

        [SerializeField] private int debugRandomNum = 0;

        [SerializeField] private WarpPointsSetting WPS;

        private Transform[] warpPoints;

        [SerializeField] private AudioClip sound1;

        [SerializeField] private AudioClip sound2;

        [SerializeField] private AudioClip sound3;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            warpPoints = WPS.points;
        }

        private void RandomAction(Transform pTrans)
        {
            int randomNum;
            if (debugRandom)
            {
                randomNum = debugRandomNum;// デバグ設定用
            }
            else
            {
                randomNum = Random.Range(0, 3);
            }
            Debug.Log("RandomNum :" + randomNum);

            switch (randomNum)
            {
                case 0:// 小さくなる
                    ParamBridge.Instance.IsShrink = true;
                    AudioManager.Instance.PlaySE(sound1);
                    break;
                case 1:// 早くなる
                    pTrans.GetComponent<PlayerCharacter>().speedUp();
                    AudioManager.Instance.PlaySE(sound2);
                    break;
                case 2:// ワープする
                    int wpNum = Random.Range(0, warpPoints.Length);
                    pTrans.position = warpPoints[wpNum].position;
                    Instantiate(particle2, pTrans.position, Quaternion.identity);
                    AudioManager.Instance.PlaySE(sound3);
                    break;
            }
        }

        void OnTriggerEnter (Collider collider)
        {
            if (collider.gameObject.CompareTag("Player"))
            {
                RandomAction(collider.transform);
                Instantiate(particle1, this.transform.position, Quaternion.identity);
                Destroy(this.transform.parent.parent.gameObject);
            }
        }



        private void OnDestroy()
        {
           //  Instantiate(particle1, this.transform.position, Quaternion.identity);
        }
    }
}
