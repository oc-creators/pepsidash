﻿//------------------------------------------------------------------------
//
// (C) Copyright 2017 Urahimono Project Inc.
//
//------------------------------------------------------------------------
using UnityEngine;
using System.Collections.Generic;
using General;

namespace Player
{
    public class SearchingBehavior_Caution : MonoBehaviour
    {
        public event System.Action<GameObject> onFound = (obj) => { };
        public event System.Action<GameObject> onLost = (obj) => { };

        [SerializeField, Range(0.0f, 360.0f)]
        private float m_searchAngle = 0.0f;
        private float m_searchCosTheta = 0.0f;

        private SphereCollider m_sphereCollider = null;
        private List<FoundData> m_foundList = new List<FoundData>();

        string str; // デバグ用

        public Finder_Caution finder;

        public MoveEnemy moveEnemy;

        private GameObject player;

        [SerializeField] private PlayerCharacter playerCharacter;

        [Range(0f, 100f)]
        public float escapeDistance = 10f;

        [SerializeField] private Transform head;

        void Start()
        {
            if (finder == null) finder = transform.GetComponentInParent<Finder_Caution>();
            if (moveEnemy == null) moveEnemy = transform.GetComponentInParent<MoveEnemy>();
            if (playerCharacter == null) playerCharacter = GameObject.FindWithTag("Player").GetComponent<PlayerCharacter>();
        }

        public float SearchAngle
        {
            get { return m_searchAngle; }
        }

        public float SearchRadius
        {
            get
            {
                if (m_sphereCollider == null)
                {
                    m_sphereCollider = GetComponent<SphereCollider>();
                }
                return m_sphereCollider != null ? m_sphereCollider.radius : 0.0f;
            }
        }


        private void Awake()
        {
            m_sphereCollider = GetComponent<SphereCollider>();
            ApplySearchAngle();
        }

        private void OnDisable()
        {
            m_foundList.Clear();
        }

        // シリアライズされた値がインスペクター上で変更されたら呼ばれます。
        private void OnValidate()
        {
            ApplySearchAngle();
        }

        private void ApplySearchAngle()
        {
            float searchRad = m_searchAngle * 0.5f * Mathf.Deg2Rad;
            m_searchCosTheta = Mathf.Cos(searchRad);
        }

        private void Update()
        {
            UpdateForward();
            UpdateFoundObject();
            Searching();
        }

        private void UpdateForward()
        {
            //this.transform.forward = new Vector3(head.forward.x, transform.forward.y, head.forward.z);
            this.transform.forward = new Vector3(head.up.x, transform.forward.y, head.up.z);// 筋肉ウサギの向いてる方向が変なため変更
        }

        private void UpdateFoundObject()
        {
            //str = "";
            foreach (var foundData in m_foundList)
            {
                GameObject targetObject = foundData.Obj;
                if (targetObject == null)
                {
                    //str += "null,  ";
                    continue;
                }
                //str += (targetObject.name + ",  ");

                bool isFound = CheckFoundObject(targetObject);
                foundData.Update(isFound);

                if (foundData.IsFound())
                {
                    onFound(targetObject);
                }
                else if (foundData.IsLost())
                {
                    onLost(targetObject);
                }
            }
            //Debug.Log(str);
        }

        private bool CheckFoundObject(GameObject i_target)
        {
            Vector3 targetPosition = playerCharacter.getHead().position;
            Vector3 myPosition = head.position;

            Vector3 myPositionXZ = Vector3.Scale(myPosition, new Vector3(1.0f, 0.0f, 1.0f));
            Vector3 targetPositionXZ = Vector3.Scale(targetPosition, new Vector3(1.0f, 0.0f, 1.0f));

            Vector3 toTargetFlatDir = (targetPositionXZ - myPositionXZ).normalized;
            Vector3 myForward = transform.forward;
            if (!IsWithinRangeAngle(myForward, toTargetFlatDir, m_searchCosTheta))
            {
                return false;
            }

            Vector3 toTargetDir = (targetPosition - myPosition).normalized;

            if (!IsHitRay(myPosition, toTargetDir, i_target))
            {
                return false;
            }

            return true;
        }

        private bool IsWithinRangeAngle(Vector3 i_forwardDir, Vector3 i_toTargetDir, float i_cosTheta)
        {
            // 方向ベクトルが無い場合は、同位置にあるものだと判断する。
            if (i_toTargetDir.sqrMagnitude <= Mathf.Epsilon)
            {
                return true;
            }

            float dot = Vector3.Dot(i_forwardDir, i_toTargetDir);
            return dot >= i_cosTheta;
        }

        private bool IsHitRay(Vector3 i_fromPosition, Vector3 i_toTargetDir, GameObject i_target)
        {
            // 方向ベクトルが無い場合は、同位置にあるものだと判断する。
            if (i_toTargetDir.sqrMagnitude <= Mathf.Epsilon)
            {
                return true;
            }

            RaycastHit onHitRay;
            if (!Physics.Raycast(i_fromPosition, i_toTargetDir, out onHitRay, SearchRadius))
            {
                return false;
            }

            if (onHitRay.transform.gameObject != i_target)
            {
                return false;
            }

            return true;
        }

        private bool IsHitRayEscapeEnemy(Vector3 i_fromPosition, Vector3 i_toTargetDir, GameObject i_target)
        {
            // 方向ベクトルが無い場合は、同位置にあるものだと判断する。
            if (i_toTargetDir.sqrMagnitude <= Mathf.Epsilon)
            {
                return true;
            }

            RaycastHit onHitRay;
            if (!Physics.Raycast(i_fromPosition, i_toTargetDir, out onHitRay, escapeDistance))
            {
                return false;
            }

            if (onHitRay.transform.gameObject != i_target)
            {
                return false;
            }

            return true;
        }

        private void OnTriggerEnter(Collider i_other)
        {
            if (!i_other.gameObject.CompareTag("Player")) return;

            GameObject enterObject = i_other.gameObject;

            // 念のため多重登録されないようにする。
            if (m_foundList.Find(value => value.Obj == enterObject) == null)
            {
                m_foundList.Add(new FoundData(enterObject));
            }
        }

        private void OnTriggerExit(Collider i_other)
        {
            GameObject exitObject = i_other.gameObject;

            var foundData = m_foundList.Find(value => value.Obj == exitObject);
            if (foundData == null)
            {
                return;
            }

            if (foundData.IsCurrentFound())
            {
                onLost(foundData.Obj);
            }

            m_foundList.Remove(foundData);
        }

        private void Searching()
        {
            //player = SearchInList();
            switch (moveEnemy.getState())
            {
                case "patrol":
                    player = SearchInList();
                    if (player != null && !ParamBridge.Instance.IsVoid)
                    {
                        moveEnemy.changeState("caution");
                        moveEnemy.setPlayerPos(player);
                    }
                    break;
                case "caution":
                    player = SearchInList();
                    moveEnemy.changeElapsedTime(player != null && !ParamBridge.Instance.IsVoid);
                    moveEnemy.setPlayerPos(player);
                    if (moveEnemy.getElapsedTime() < 0)
                    {
                        moveEnemy.changeState("patrol");
                    }
                    else if (moveEnemy.getElapsedTime() > moveEnemy.getCautionWaitTime())
                    {
                        moveEnemy.changeState("chase");
                    }
                    break;
                case "chase":
                    moveEnemy.setPlayerPos(player);
                    /*
                    if (player == null)
                    {
                        moveEnemy.changeState("warning");
                    } else
                    {
                        Vector3 toPlayerDir = (player.transform.position - transform.parent.position).normalized;
                        if (!IsHitRayEscapeEnemy(transform.parent.position, toPlayerDir, player)) moveEnemy.changeState("warning");
                        //if (player == null) moveEnemy.changeState("warning");
                    }
                    */
                    Vector3 toPlayerDir = (playerCharacter.getHead().position - head.position).normalized;
                    if (!IsHitRayEscapeEnemy(head.position, toPlayerDir, player) || ParamBridge.Instance.IsVoid) moveEnemy.changeState("warning");
                    break;
                case "warning":
                    player = SearchInList();
                    if (player != null && !ParamBridge.Instance.IsVoid)
                    {
                        moveEnemy.changeState("chase");
                    } 
                    break;
            }
        }

        private GameObject SearchInList()
        {
            foreach (GameObject target in finder.getM_targets())
            {
                if (target.CompareTag("Player") && !ParamBridge.Instance.IsVoid) return target;
            }
            return null;
        }

        public void setPlayer(GameObject obj)
        {
            player = obj;
        }

        private class FoundData
        {
            public FoundData(GameObject i_object)
            {
                m_obj = i_object;
            }

            private GameObject m_obj = null;
            private bool m_isCurrentFound = false;
            private bool m_isPrevFound = false;

            public GameObject Obj
            {
                get { return m_obj; }
            }

            public Vector3 Position
            {
                get { return Obj != null ? Obj.transform.position : Vector3.zero; }
            }

            public void Update(bool i_isFound)
            {
                m_isPrevFound = m_isCurrentFound;
                m_isCurrentFound = i_isFound;
            }

            public bool IsFound()
            {
                return m_isCurrentFound && !m_isPrevFound;
            }

            public bool IsLost()
            {
                return !m_isCurrentFound && m_isPrevFound;
            }

            public bool IsCurrentFound()
            {
                return m_isCurrentFound;
            }
        }

    } // class SearchingBehavior
}
