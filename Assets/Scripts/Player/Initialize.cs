﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Initialize : MonoBehaviour
    {

        private Vector3 firstPosition;

        private Vector3 firstForward;


        // Start is called before the first frame update
        void Start()
        {
            firstPosition = transform.position;
            firstForward = transform.forward;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Init()
        {
            transform.position = firstPosition;
            transform.forward = firstForward;
        }
    }
}
