﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Player
{
    public class Smash : MonoBehaviour
    {
        // private Rigidbody rb;

        [SerializeField] [Range(0f, 50f)] private float forceRate = 1f;
        [SerializeField] [Range(0f, 50f)] private float upRate = 1f;

        [SerializeField] private AudioClip sound;

        // Start is called before the first frame update
        void Start()
        {
            // rb = this.GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Vector3 forward = collision.transform.position - this.transform.position;
                Rigidbody prb = collision.transform.GetComponent<Rigidbody>();
                forward.Normalize();
                forward += new Vector3(0, 1, 0);
                prb.AddForce(forward * forceRate, ForceMode.Impulse);
                ParamBridge.Instance.IsDead = true;
                GameManager.Instance.EndGame();
                AudioManager.Instance.PlaySE(sound);
                // Debug.Log("Forward :" + forward);
                // Debug.Log("Hit Player!!");
            }
        }
    }
}
