﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UserInterface{
	
	public class Stagemove_left : MonoBehaviour
	{
		public RectTransform stage1;
		public RectTransform stage2;
		//public RectTransform stage3;

		private int counter = 0;
		//private int click = 0;
		private float move = -Screen.width/300f;
		bool left = false;
		public static bool stage1select = true;

		public void Start(){
				Debug.Log("Screen Width : " + Screen.width);
				Debug.Log("Screen  height: " + Screen.height);
		}

		public void OnClick(){
				left = true;
		}

		void Update(){

			if(left==true && stage1select){
				stage1.position += new Vector3(move,0,0);
				stage2.position += new Vector3(move,0,0);
				//stage3.position += new Vector3(move,0,0);
				//counter++;
				//Debug.Log(stage2.position.x);
				if(stage2.position.x <= Screen.width/2){
					stage1.position += new Vector3(0,0,0);
					stage2.position += new Vector3(0,0,0);
					//stage3.position += new Vector3(0,0,0);
					//counter = 0;
					left = false;
					stage1select = false;
					Stagemove_right.stage2select = true;
				}
			}else{
				left = false;
			}
		}
	}
}