﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UserInterface{
	
	public class Stagemove_right : MonoBehaviour
	{
		public RectTransform stage1;
		public RectTransform stage2;
		public RectTransform stage3;

		private int counter = 0;
		//private int x = Screen.width/100
		private float move = Screen.width/300f;
		bool right = false;
		public static bool stage2select = false;
		//public static bool stage3select = false;

		public void Start(){
			Debug.Log("Screen Width : " + Screen.width);
			Debug.Log("Screen  height: " + Screen.height);
		}

		public void OnClick(){
			right = true;
		}

		void Update(){
			
			if(right==true && stage2select){
				stage1.position += new Vector3(move,0,0);
				stage2.position += new Vector3(move,0,0);
				//stage3.position += new Vector3(move,0,0);
				//counter++;
				//Debug.Log(stage1.position.x);

				if(stage1.position.x >= Screen.width/2){
					stage1.position += new Vector3(0,0,0);
					stage2.position += new Vector3(0,0,0);
					stage3.position += new Vector3(0,0,0);
					counter = 0;
					right = false;
					stage2select = false;
					Stagemove_left.stage1select = true;
				}
			}else{
				right=false;
			}
		}
	}
}