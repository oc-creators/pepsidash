﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

public class TimeController : MonoBehaviour
{
	float seconds;
	float bubble_seconds;
	public GameObject Bubble1;
	public GameObject Bubble2;
	public GameObject Busy_bubble1;
	public GameObject Busy_bubble2;
	public GameObject Busy_bubble3;
	public GameObject Void_cool;
	public GameObject Stand;
	public GameObject Crouch;

	void Update()
	{
		seconds += Time.deltaTime;

		if (seconds >= 2.5)
		{
			Bubble1.SetActive(true);
		}
		if (seconds >= 3.0)
		{
			Bubble2.SetActive(true);
		}
		if (bubble_seconds >= 0.5)
		{
			Busy_bubble1.SetActive(true);
		}
		if (bubble_seconds >= 0.8)
		{
			Busy_bubble2.SetActive(true);
		}
		if (bubble_seconds >= 1.0)
		{
			Busy_bubble3.SetActive(true);
		}
		if (ParamBridge.Instance.IsVoid)
		{
			bubble_seconds += Time.deltaTime;
			//Debug.Log(bubble_seconds);
		}
		else
		{
			Busy_bubble1.SetActive(false);
			Busy_bubble2.SetActive(false);
			Busy_bubble3.SetActive(false);
			bubble_seconds = 0;

		}
		if (ParamBridge.Instance.IsCoolTime || ParamBridge.Instance.CountIntoVoid==0)
		{
			Void_cool.SetActive(true);
		}
		if (ParamBridge.Instance.IsCoolTime==false)
		{
			Void_cool.SetActive(false);
		}
		if (ParamBridge.Instance.IsCrouch && ParamBridge.Instance.IsVoid==false)
		{
			Stand.SetActive(true);
			Crouch.SetActive(false);
		}
		if (ParamBridge.Instance.IsCrouch==false && ParamBridge.Instance.IsVoid==false)
		{
			Stand.SetActive(false);
			Crouch.SetActive(true);
		}
		/*if (ParamBridge.Instance.IsCrouch && ParamBridge.Instance.IsVoid)
		{
			Stand.SetActive(false);
			Crouch.SetActive(true);
		}*/
			//bubble_seconds = 0;
		//Debug.Log(bubble_seconds);
	}
}