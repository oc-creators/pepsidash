﻿using System;
using UnityEngine;
using UnityEngine.UI;
using General;

namespace UserInterface {

	public class UIManager : SingletonMonoBehaviour<UIManager> {
		protected override bool dontDestroyOnLoad { get { return false; } }
		public Image bottleImage;
		private GameFlowController gfc;
		private ParamBridge pb;
		private Gradation gradation;
		[SerializeField] private GameObject chasedFrame;

		private int prevFrameCount = ParamBridge.INIT_FRAME;
		private float prevElapsed = ParamBridge.INIT_ELAPSED;
		private float elapsed = 0f;
		private int fps = 0;

		// 何秒ごとに温度値を消費するか
		private const float SPENDING_TEMP_RATE = 6f;
		// 消費温度値
		private const int SPENDING_TEMP_VALUE = 1;
		// 消費炭酸値
		private const int SPENDING_GAS_VALUE = 20;


		// 初期化
		protected override void Start()
		{
			gfc = GameFlowController.Instance;
			pb = ParamBridge.Instance;
			gradation = bottleImage.GetComponent<Gradation>();
		}

		// 更新
		protected override void Update()
		{
			if (gfc.VMode < ViewMode.GameEntry || pb.IsOver)
            {
				return;
            }

			// TODO: 虚空使ったら gas -= 20, 使用中に減るように

			elapsed = pb.Elapsed - prevElapsed;
			// 時間経過で温度値減少
			if (elapsed >= SPENDING_TEMP_RATE)
			{
				pb.Temp += SPENDING_TEMP_VALUE;
				prevElapsed = pb.Elapsed;
				elapsed = 0f;
			}
            // UI更新
            //gradation.gradiate(Math.Min(1f, (float)pb.Temp / ParamBridge.TEMP_MAX));

			if(pb.IsChase && !chasedFrame.activeSelf)
			{
				chasedFrame.SetActive(true);
			}
			if(!pb.IsChase && chasedFrame.activeSelf)
			{
				chasedFrame.SetActive(false);
			}

		}
#if DEBUG
		private void OnGUI()
        {
			Vector2 guiScreenSize = new Vector2(640, 360);  // 基準とする解像度
			GUIUtility.ScaleAroundPivot(new Vector2(Screen.width / guiScreenSize.x, Screen.height / guiScreenSize.y), Vector2.zero);
			GUILayout.BeginArea(new Rect(50, 150, 300, 200));
			GUILayout.Label($"Elapsed: {(int)pb.Elapsed}");
			GUILayout.Label($"TEMP: {pb.Temp}℃");
			GUILayout.Label($"GAS: {pb.Gas}％");
			GUILayout.EndArea();
		}
#endif
	}
}