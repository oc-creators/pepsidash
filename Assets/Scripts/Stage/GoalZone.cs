﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Stage
{
    public class GoalZone : MonoBehaviour
    {
        [SerializeField] private GameManager gm;
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player")) 
            {
                ParamBridge.Instance.Reached = true;
                gm.EndGame();
                Debug.Log("Goal!!");
            }
        }

    }
}
