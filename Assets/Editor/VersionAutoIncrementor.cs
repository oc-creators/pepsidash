﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using System;

public class VersionAutoIncrementor
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        var json_path = $"{Application.dataPath}/Resources/Data/info.json";
        var info = new General.Info();

        switch (target)
        {
            case BuildTarget.Android:
                var versionCode = PlayerSettings.Android.bundleVersionCode + 1;
                PlayerSettings.Android.bundleVersionCode = versionCode;
                info.version = $"{PlayerSettings.bundleVersion}.{versionCode}";
                info.buildAt = DateTime.Now.ToString("yyyyMMdd-HHmm");

                break;

            case BuildTarget.iOS:
                var buildNumber = int.Parse(PlayerSettings.iOS.buildNumber) + 1;
                PlayerSettings.iOS.buildNumber = "" + buildNumber;
                break;

            default:
                break;

        }

        General.JsonManager.Check(json_path);
        General.JsonManager.Dump(ref info, json_path);
    }
}